# [PizzaLovingNerd/ice](https://github.com/PizzaLovingNerd/ice) PKGBUILD
I made this PKGBUILD because I use PizzaLovingNerd's version of Ice on my PinePhone running Arch Linux ARM a lot. It's one of the first applications I install when I reflash my SD card or eMMC. I got tired of manually copying the files/directories every time and I wanted to easily be able to update it or remove it if needed.

## Obtaining
The only necessary file is the pkg.tar.xz file in the root directory of the repository. You can download this individually in GitLab by clicking on it in the [project overview](https://gitlab.com/nickgirga/ice) and clicking the download button. You may also click on [releases](https://gitlab.com/nickgirga/ice/-/releases) and download the latest compressed archive (zip, tar.gz, tar.bz2, tar) and decompress it or you can clone the repository by running `git clone https://gitlab.com/nickgirga/ice.git` if you wish to grab the PKGBUILD as well.

## Building
This step is not strictly necessary. A pre-built package is included in the repository. If you wish to rebuild it yourself, run `makepkg -f` in the same directory as the PKGBUILD file.

## Installation
You should be able to simply open the pkg.tar.xz file in a GUI package manager and click or tap install (only tested with [pamac-aur](https://aur.archlinux.org/packages/pamac-aur/)). To install the package in the command line, you can run `makepkg -i` or `pacman -U [package_name].pkg.tar.xz` in the same directory as the pkg.tar.xz file—replacing `[package_name]` with the name of your package file. If you wish to rebuild the package before installing it, the `makepkg -fi` command will do it all in one line.

## Removal
You can search for `pln-ice-git` in your GUI package manager to remove it or you can run `pacman -Rns pln-ice-git`.

## Screenshots
![ice.png](.screenshots/ice.png)
![pamac.png](.screenshots/pamac.png)
